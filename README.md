# edid

After having prepared your EDID place it in a folder, e.g. called edid under /usr/lib/firmware and copy your binary into it.

To load it at boot, specify the following in the kernel command line:

drm_kms_helper.edid_firmware=edid/your_edid.bin

or alternatively (since kernel 4.15), one may also enforce the EDID information on a lower level, using:

drm.edid_firmware=edid/your_edid.bin

In order to apply it only to a specific monitor use:

drm_kms_helper.edid_firmware=VGA-1:edid/your_edid.bin

for tpx240:
drm.edid_firmware=eDP1:edid/edid_tpx240.bin